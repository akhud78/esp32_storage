#pragma once
#include <stdint.h>
#include <esp_err.h>

#define SDCARD_BASE_PATH    "/sdcard"

#ifdef __cplusplus
extern "C" {
#endif

esp_err_t sdcard_mount(void);
void sdcard_unmount(void);

void sdcard_usage(uint64_t* total_bytes, uint64_t* free_bytes);
//int sdcard_directory_print(const char* directory_name);

#ifdef __cplusplus
}
#endif

