// https://github.com/tuupola/esp_video/blob/master/components/esp_sdcard/sdcard.c
#include <esp_log.h>
#include <esp_err.h>
#include "esp_vfs.h"
#include <esp_vfs_fat.h>
#include <driver/sdspi_host.h>
#include <driver/sdmmc_host.h>
#include <sdmmc_cmd.h>
#include <string.h>

#include "sdkconfig.h"
#include "sdcard.h"

static const char *TAG = "sd";
static sdmmc_card_t* s_card = NULL;

// https://github.com/espressif/esp-idf/blob/release/v4.4/examples/storage/ext_flash_fatfs/main/ext_flash_fatfs_example_main.c
// https://github.com/espressif/esp-idf/blob/release/v4.4/components/fatfs/src/ff.h
void sdcard_usage(uint64_t* total_bytes, uint64_t* free_bytes)
{
    FATFS *fs;
    size_t free_clusters;
    
#if ESP_IDF_VERSION >= ESP_IDF_VERSION_VAL(5, 0, 0)
    int res = f_getfree("0:", (long unsigned int *)(&free_clusters), &fs); // 0: - Logical drive number
#else  // IDF v4.4
    int res = f_getfree("0:", (unsigned int *)(&free_clusters), &fs);
#endif
    
    assert(res == FR_OK);
    uint64_t total_sectors = (fs->n_fatent - 2) * fs->csize;
    uint64_t free_sectors = free_clusters * fs->csize;

    // with uint64_t total size may be > 4GiB
    if (total_bytes != NULL) {
        *total_bytes = total_sectors * fs->ssize; // ssize - sector size = 512
    }
    if (free_bytes != NULL) {
        *free_bytes = free_sectors * fs->ssize;
    }
}

// https://github.com/espressif/esp-idf/tree/release/v4.4/examples/storage/sd_card
esp_err_t sdcard_mount(void)
{
    esp_err_t ret;

    const esp_vfs_fat_sdmmc_mount_config_t mount_config = {
#ifdef CONFIG_STORAGE_FORMAT_IF_MOUNT_FAILED
        .format_if_mount_failed = true,
#else
        .format_if_mount_failed = false,
#endif // CONFIG_STORAGE_FORMAT_IF_MOUNT_FAILED
        .max_files = 5,
        .allocation_unit_size = 16 * 1024
    };

    ESP_LOGI(TAG, "Initializing SD card");


#ifdef CONFIG_STORAGE_SDMMC_MODE
    ESP_LOGI(TAG, "Using SDMMC peripheral");
    sdmmc_host_t host = SDMMC_HOST_DEFAULT();
    
    // This initializes the slot without card detect (CD) and write protect (WP) signals.
    // Modify slot_config.gpio_cd and slot_config.gpio_wp if your board has these signals.
    sdmmc_slot_config_t slot_config = SDMMC_SLOT_CONFIG_DEFAULT();
            
    // On chips where the GPIOs used for SD card can be configured, set them in
    // the slot_config structure:
    slot_config.width = 1;
    slot_config.clk = CONFIG_STORAGE_SDMMC_PIN_CLK;
    slot_config.cmd = CONFIG_STORAGE_SDMMC_PIN_CMD;
    slot_config.d0 = CONFIG_STORAGE_SDMMC_PIN_D0;
    
#ifdef CONFIG_STORAGE_SDMMC_BUS_WIDTH_4
    slot_config.width = 4;
    slot_config.d1 = CONFIG_STORAGE_SDMMC_PIN_D1;
    slot_config.d2 = CONFIG_STORAGE_SDMMC_PIN_D2;
    slot_config.d3 = CONFIG_STORAGE_SDMMC_PIN_D3;
#endif  // CONFIG_STORAGE_SDMMC_BUS_WIDTH_4
    
    // Enable internal pullups on enabled pins. The internal pullups
    // are insufficient however, please make sure 10k external pullups are
    // connected on the bus. This is for debug / example purpose only.
    slot_config.flags |= SDMMC_SLOT_FLAG_INTERNAL_PULLUP;
    
    ret = esp_vfs_fat_sdmmc_mount(SDCARD_BASE_PATH, &host, &slot_config, &mount_config, &s_card);
    
#endif // CONFIG_STORAGE_SDMMC_MODE
    
#ifdef CONFIG_STORAGE_SDSPI_MODE
    ESP_LOGI(TAG, "Using SPI peripheral");

    sdmmc_host_t host = SDSPI_HOST_DEFAULT();
    spi_bus_config_t bus_cfg = {
        .mosi_io_num = CONFIG_STORAGE_SDSPI_PIN_MOSI,
        .miso_io_num = CONFIG_STORAGE_SDSPI_PIN_MISO, 
        .sclk_io_num = CONFIG_STORAGE_SDSPI_PIN_CLK, 
        .quadwp_io_num = -1,
        .quadhd_io_num = -1,
        .max_transfer_sz = 4000,
    };
    ret = spi_bus_initialize(host.slot, &bus_cfg, SPI_DMA_CH_AUTO); // SPI_DMA_CHAN=1
    if (ret != ESP_OK) {
        ESP_ERROR_CHECK_WITHOUT_ABORT(ret);
        return ret;
    }

    // This initializes the slot without card detect (CD) and write protect (WP) signals.
    // Modify slot_config.gpio_cd and slot_config.gpio_wp if your board has these signals.
    sdspi_device_config_t slot_config = SDSPI_DEVICE_CONFIG_DEFAULT();
    slot_config.gpio_cs = CONFIG_STORAGE_SDSPI_PIN_CS; // 1
    slot_config.host_id = host.slot;

    ret = esp_vfs_fat_sdspi_mount(SDCARD_BASE_PATH, &host, &slot_config, &mount_config, &s_card);

#endif // CONFIG_STORAGE_SDSPI_MODE

    if (ret != ESP_OK) {
        ESP_ERROR_CHECK_WITHOUT_ABORT(ret);
        return ret;
    }

    // Card has been initialized, print its properties
    sdmmc_card_print_info(stdout, s_card);
        
    return ESP_OK;
}

void sdcard_unmount(void)
{
    // All done, unmount partition and disable SDMMC peripheral
    esp_vfs_fat_sdcard_unmount(SDCARD_BASE_PATH, s_card);

#ifdef CONFIG_STORAGE_SDSPI_MODE
    sdmmc_host_t host = SDSPI_HOST_DEFAULT();
    esp_err_t ret = spi_bus_free(host.slot);
    if (ret != ESP_OK) {
        ESP_ERROR_CHECK_WITHOUT_ABORT(ret);
    }
#endif // CONFIG_STORAGE_SDSPI_MODE
}
