# esp32_storage
Storage component


## Setup
- Add [storage component](https://gitlab.com/akhud78/esp32_storage) into `components` folder.
```
$ cd ~/esp/my_project/components
$ git submodule add https://gitlab.com/akhud78/esp32_storage storage
```

## Configuration
- [Kconfig](Kconfig)

### SD SPI mode
```
[ ] Format the card if mount failed
    SD mode (Use SD card with SPI interface)  --->
(4) MOSI GPIO number
(6) MISO GPIO number
(5) CLK GPIO number
(7) CS GPIO number
```

### SD MMC mode (1-line)
```
[ ] Format the card if mount failed
    SD mode (Use SD card with MMC interface)  --->
    SD/MMC bus width (1 line (D0))  --->
(38) CMD GPIO number (NEW)
(39) CLK GPIO number (NEW)
(40) D0 GPIO number (NEW)
```
## Test
- SD SPI mode
```
Running sdcard file write run time...
...
I (38703) STO: _file_write '/sdcard/test.dat'
I (38913) STO: Write OK: 211 ms bytes: 102400 rate: 485000 bytes/s
time: 239 ms bytes: 102400 rate: 428000 bytes/s
I (38943) STO: _file_remove '/sdcard/test.dat'
I (38963) STO: _sdcard_unmount
```
- SD MMC mode (1-line)
```
Running sdcard file write run time...
...
I (13662) STO: _file_write '/sdcard/test.dat'
I (13932) STO: Write OK: 261 ms bytes: 102400 rate: 392000 bytes/s
I (13962) STO: _file_remove '/sdcard/test.dat'
I (13982) STO: _sdcard_unmount
```