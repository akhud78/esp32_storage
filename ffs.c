// SPI Flash File System - spiffs
// https://github.com/espressif/esp-idf/blob/release/v4.4/examples/storage/spiffs/main/spiffs_example_main.c
#include <esp_log.h>
#include "esp_vfs.h"
#include "esp_spiffs.h"

#include "sdkconfig.h"
#include "ffs.h"

static const char *TAG = "ffs";


esp_err_t ffs_mount(void)
{
    esp_vfs_spiffs_conf_t conf = {
      .base_path = FFS_BASE_PATH,
      .partition_label = FFS_PARTITION,
      .max_files = 5,
      .format_if_mount_failed = true
    };
    
    // Use settings defined above to initialize and mount SPIFFS filesystem.
    // Note: esp_vfs_spiffs_register is an all-in-one convenience function.
    esp_err_t ret = esp_vfs_spiffs_register(&conf);

    if (ret != ESP_OK) {
        if (ret == ESP_FAIL) {
            ESP_LOGE(TAG, "Failed to mount or format filesystem");
        } else if (ret == ESP_ERR_NOT_FOUND) {
            ESP_LOGE(TAG, "Failed to find SPIFFS partition");
        } else {
            ESP_LOGE(TAG, "Failed to initialize SPIFFS (%s)", esp_err_to_name(ret));
        }
        return ret;
    }
        

    size_t total = 0, used = 0;
    ret = esp_spiffs_info(conf.partition_label, &total, &used);
    if (ret != ESP_OK) {
        ESP_LOGE(TAG, "Failed to get SPIFFS partition information (%s)", esp_err_to_name(ret));
    } else {
        ESP_LOGI(TAG, "Partition size: total: %d, used: %d", total, used);
    }
    // ...
    
    return ESP_OK;
}

void ffs_unmount(void)
{
    // All done, unmount partition and disable SPIFFS
    esp_vfs_spiffs_unregister(FFS_PARTITION);
}

void ffs_usage(size_t* total_bytes, size_t* free_bytes)
{
    size_t used = 0;
    size_t total = 0;
    
    esp_spiffs_info(FFS_PARTITION, &total, &used);
    *total_bytes = total;
    *free_bytes = total - used;
}

esp_err_t ffs_format(void)
{
    esp_err_t ret = esp_spiffs_format(FFS_PARTITION);
    if (ret != ESP_OK) {
        ESP_LOGE(TAG, "Failed to format SPIFFS partition (%s)", esp_err_to_name(ret));
    } else {
        ESP_LOGI(TAG, "Format OK");
    }
    return ret;
}





