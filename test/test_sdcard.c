#include <string.h>
#include "esp_vfs.h"
#include "esp_timer.h"
#include "unity.h"
#include "storage.h"
#include "sdcard.h"


TEST_CASE("fat mount", "[sdcard]")
{
    TEST_ASSERT_EQUAL(ESP_OK, sdcard_mount()); 
    
    uint64_t total_bytes = 0;
    uint64_t free_bytes = 0;
    sdcard_usage(&total_bytes, &free_bytes);
    printf("total_bytes: %llu kb, free_bytes: %llu kb\n", total_bytes / 1024, free_bytes / 1024);
    
    storage_dir_read(SDCARD_BASE_PATH);
    
    sdcard_unmount();
}

TEST_CASE("fat write read remove", "[sdcard]")
{
    TEST_ASSERT_EQUAL(ESP_OK, sdcard_mount()); 

    char dir[] = SDCARD_BASE_PATH "/tmp";
    char path[] = SDCARD_BASE_PATH "/tmp/test.txt";
    
    // Create directory
    struct stat st = {0};
    int ret = stat(dir, &st);
    if (ret == 0 && S_ISDIR(st.st_mode)) {  // directory exists
        TEST_FAIL_MESSAGE("Directory exists");
    } 
    mkdir(dir, 0755);
    
    // Write file
    FILE *fw = fopen(path, "wb");
    if (fw) {
        char data[] = "12345";
        size_t len = strlen(data);
        fwrite(data, 1, len, fw);
        fclose(fw);
    }
    TEST_ASSERT_NOT_NULL(fw);
    
    // Read file
    FILE *fr = fopen(path, "rb");
    if (fr) {
        char data[6] = "";
        size_t len = 5;
        fread(data, 1, len, fr);
        printf("data: %s\n", data);
        fclose(fr);        
    }
    TEST_ASSERT_NOT_NULL(fr);
    
    // Remove file
    int status = -1;
    // Check if destination file exists
    ret = stat(path, &st);
    if (ret == 0) {     // file found
        printf("%s - %ld bytes\n", path, st.st_size); // file size
        status = unlink(path);      // 0 - OK, -1 - ERROR
    }
    TEST_ASSERT_EQUAL(0, status);
    
    // Remove directory (must be empty)
    // Check if destination directory exists
    if (stat(dir, &st) == 0) {
        status = rmdir(dir);
    }
    TEST_ASSERT_EQUAL(0, status);
    
    sdcard_unmount();
}

// Test writing time
TEST_CASE("fat write run time", "[sdcard]")
{    
    TEST_ASSERT_EQUAL(ESP_OK, sdcard_mount());

    const size_t buf_len = 1024 * 100; // 100k
    size_t bytes = 0;
    uint8_t *buf = calloc(buf_len, sizeof(uint8_t)); // all zeros

    uint64_t start = esp_timer_get_time();  // Get time in microseconds since boot
    // Write file
    FILE *fp = fopen(SDCARD_BASE_PATH  "/test.dat", "wb");
    if (fp) {
        bytes = fwrite(buf, 1, buf_len, fp);
        fclose(fp);
    }
    uint64_t end = esp_timer_get_time();
    TEST_ASSERT_NOT_NULL(fp);
    TEST_ASSERT_EQUAL(bytes, buf_len);
    
    free(buf);
    unlink(SDCARD_BASE_PATH  "/test.dat"); // remove file
    sdcard_unmount();
    
    uint64_t run_time_ms = (end - start)/1000;
    printf("time: %llu ms bytes: %d rate: %llu bytes/s\n", run_time_ms, buf_len, buf_len/run_time_ms*1000 );
    // SDMMC 1-bit mode     time: 198 ms bytes: 102400 rate: 517000 bytes/s
    // SPI mode             time: 240 ms bytes: 102400 rate: 426000 bytes/s
}
