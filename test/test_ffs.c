#include <string.h>
#include "esp_vfs.h"

#include "unity.h"
#include "sdcard.h"
#include "storage.h"
#include "ffs.h"


TEST_CASE("spiffs mount", "[ffs]")
{
    TEST_ASSERT_EQUAL(ESP_OK, ffs_mount()); 
    
    storage_dir_read(SDCARD_BASE_PATH);
    
    ffs_unmount();
}

TEST_CASE("spiffs format - all your data will be lost!", "[format]")
{
    TEST_ASSERT_EQUAL(ESP_OK, ffs_mount()); 
    
    TEST_ESP_OK(ffs_format());
    
    storage_dir_read(SDCARD_BASE_PATH);
    
    ffs_unmount();
}


TEST_CASE("spiffs write read remove", "[ffs]")
{
    TEST_ASSERT_EQUAL(ESP_OK, ffs_mount()); 

    char path[] = FFS_BASE_PATH "/tmp/test.txt";
    
    // Create directory    
    // Write file
    FILE *fw = fopen(path, "wb");
    if (fw) {
        char data[] = "12345";
        size_t len = strlen(data);
        fwrite(data, 1, len, fw);
        fclose(fw);
    }
    TEST_ASSERT_NOT_NULL(fw);
    
    // Read file
    FILE *fr = fopen(path, "rb");
    if (fr) {
        char data[6] = "";
        size_t len = 5;
        fread(data, 1, len, fr);
        printf("data: %s\n", data);
        fclose(fr);        
    }
    TEST_ASSERT_NOT_NULL(fr);
    
    // Remove file
    int status = -1;
    struct stat st = {0};
    // Check if destination file exists
    if (stat(path, &st) == 0) {     // file found
        printf("%s - %ld bytes\n", path, st.st_size); // file size
        status = unlink(path);      // 0 - OK, -1 - ERROR
    }
    TEST_ASSERT_EQUAL(0, status);
    
    ffs_unmount();
}

