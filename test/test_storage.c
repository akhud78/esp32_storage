#include <sys/time.h>
#include <string.h>
#include "esp_timer.h"
#include "unity.h"
#include "nvs_flash.h"

#include "sdcard.h"
#include "ffs.h"
#include "storage.h"


TEST_CASE("fat write rename read remove", "[storage]")
{
    TEST_ESP_OK(sdcard_mount()); 
    char path[] = SDCARD_BASE_PATH "/test.dat";
    char data[] = "hello world!";
    
    int len = strlen(data);
    int bytes = storage_write(path, (uint8_t *)data, len, 0);
    
    TEST_ASSERT_EQUAL(len, bytes);
    
    char dest[] = SDCARD_BASE_PATH "/dummy.dat";

    TEST_ASSERT_EQUAL(0, storage_file_rename(path, dest));

    bytes = storage_file_size(dest);
    TEST_ASSERT_EQUAL(len, bytes);
    
    uint8_t buf[100];
    bytes = storage_read(dest, buf, len, 0);
    TEST_ASSERT_EQUAL(len, bytes);
    
    TEST_ASSERT_EQUAL(0, storage_file_remove(dest));
    
    storage_dir_read(SDCARD_BASE_PATH);
    sdcard_unmount();
}

TEST_CASE("fat dir create clear remove", "[storage]")
{
    TEST_ESP_OK(sdcard_mount()); 
    char path_dir[] = SDCARD_BASE_PATH "/foo";
    
    TEST_ASSERT_EQUAL(0, storage_dir_create(path_dir));
    TEST_ASSERT_EQUAL(0, storage_dir_create(path_dir)); // create again! no error
    
    char path_file[] = SDCARD_BASE_PATH "/foo/test.dat";
    char data[] = "hello world!";
    
    int len = strlen(data);
    int bytes = storage_write(path_file, (uint8_t *)data, len, 0);
    
    TEST_ASSERT_EQUAL(len, bytes);
    storage_dir_read(path_file);
    
    TEST_ASSERT_EQUAL(0, storage_dir_clear(path_dir));
    TEST_ASSERT_EQUAL(0, storage_dir_remove(path_dir));
    
    sdcard_unmount();
}


TEST_CASE("spiffs dir read", "[storage]")
{
    TEST_ESP_OK(ffs_mount()); 
    
    char file_name[32] = "";
    char dir_name[] = FFS_BASE_PATH "/foo";
    char data[] = "hello world!";
    int len = strlen(data);
    int imax = 3; 
    
    printf("len: %d\n", len);
    
    // create files
    for (int i=0; i<imax; i++) {
        sprintf(file_name, "%s/%08x.txt", dir_name, i);  // foo/00000002.txt
        int bytes = storage_write(file_name, (uint8_t *)data, len, 0);
        if (bytes != len)
            break;
    }
    
    int files_in_dir = storage_dir_read(dir_name);
    
    // delete files
    for (int i=0; i<imax; i++) {
        sprintf(file_name, "%s/%08x.txt", dir_name, i); 
        esp_err_t res = storage_file_remove(file_name);
        if (res != ESP_OK)
            break;
    }

    storage_dir_read(dir_name);
    
    ffs_unmount();
    TEST_ASSERT_EQUAL(files_in_dir, imax);
}


// --- NVS ---
// https://github.com/espressif/esp-idf/tree/v4.4/examples/storage/nvs_rw_value
// https://github.com/espressif/esp-idf/blob/v4.4/components/nvs_flash/nvs_partition_generator/sample_singlepage_blob.csv
/*
  ...
  Read value = abcdefghijklmnopqrstuvwxyz  
  Write value = 7545935
*/

TEST_CASE("str read write", "[nvs]")
{    

    // Initialize the default NVS partition.    
    esp_err_t err = storage_nvs_init();
    TEST_ASSERT_EQUAL(ESP_OK, err);
    
    const char key[] = "dummyStringKey";  
    
    char value[30] = "";
    err = storage_nvs_read_str(key, value, sizeof(value)-1);
    switch (err) {
        case ESP_OK:
            printf("Read value = %s\n", value);
            break;
        case ESP_ERR_NVS_NOT_FOUND:
            printf("The value is not initialized yet!\n");
            break;
        default :
            printf("Error (%s) reading!\n", esp_err_to_name(err));
    }
    //TEST_ASSERT_EQUAL(ESP_OK, err);

    uint64_t time_value = esp_timer_get_time();  // Get time in microseconds since boot
    sprintf(value, "%llu", time_value);
    err = storage_nvs_write_str(key, value);
    switch (err) {
        case ESP_OK:
            printf("Write value = %s\n", value);
            break;
        default :
            printf("Error (%s) writing!\n", esp_err_to_name(err));
    }
    TEST_ASSERT_EQUAL(ESP_OK, err);    
    TEST_ASSERT_EQUAL(ESP_OK, nvs_flash_deinit());
}

