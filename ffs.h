#pragma once
// SPI Flash File System - spiffs
#include <stdint.h>
#include <esp_err.h>


#define FFS_BASE_PATH       "/spiffs"
#define FFS_PARTITION       "storage"

#ifdef __cplusplus
extern "C" {
#endif

esp_err_t ffs_mount(void);
void ffs_unmount(void);
void ffs_usage(size_t* total_bytes, size_t* free_bytes);

esp_err_t ffs_format(void);

#ifdef __cplusplus
}
#endif
