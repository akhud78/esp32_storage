// https://github.com/espressif/esp-idf/blob/release/v4.4/examples/storage/sd_card/sdmmc/main/sd_card_example_main.c

#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "esp_system.h"
#include "esp_log.h"
#include "sdkconfig.h"

#include "esp_timer.h"
#include "sdmmc_cmd.h"
#include "driver/sdmmc_host.h"
#include "esp_spiffs.h"
#include "esp_vfs.h"
#include "esp_vfs_fat.h"
#include "esp_partition.h"
#include "nvs_flash.h"

#include "storage.h"

static const char *TAG = "sto";


// Create directory
int storage_dir_create(const char* path)
{
    struct stat st = {};
    if (stat(path, &st) == 0) {  // exists
        if (S_ISDIR(st.st_mode)) { // directory
            ESP_LOGW(TAG, "Directory '%s' exist", path);
            return 0;
        }
        return -1; // not directory
    } 
    return mkdir(path, 0755);
}

// Remove directory (must be empty)
int storage_dir_remove(const char* path)
{
    // Check if destination directory exists
    struct stat st = {};
    if (stat(path, &st) != 0) {
        ESP_LOGE(TAG, "Directory '%s' Not Found", path);
        return -1;
    }
    
    return rmdir(path);
}

int storage_dir_clear(const char* path)
{        
    DIR *dir = opendir(path);
    if (!dir) {
        return -1;
    }

    struct dirent *entry;
    int res = 0;

    while ( (entry = readdir(dir)) != NULL) {
        if (entry->d_type != DT_REG) // not regular file
            continue;
            
        char file_name[32] = "";
        strcat(file_name, path); // dir_name
        strcat(file_name, "/");
        strcat(file_name, entry->d_name);
        
        res = storage_file_remove(file_name);
        if (res) {
            break;  // res = -1 exit with error 
        }
    }

    closedir(dir);
    
    return res;
}

int storage_dir_read(const char* path)
{
    int files_in_dir = 0;
    const int files_to_print = 10; 
        
    DIR *dir = opendir(path);
    if (!dir) {
        return -1;
    }

    struct dirent *entry;
    char mes[300] = "";
    while ( (entry = readdir(dir)) != NULL) {
        if (entry->d_type != DT_REG) // not regular file
            continue;
            
        char file_name[32] = "";
        if (strlen(path)) { // dir_name
            strcat(file_name, path);
            strcat(file_name, "/");
        }
        strcat(file_name, entry->d_name);
    
        int size = storage_file_size(file_name);
        sprintf(mes, "%s\t%d\n", entry->d_name, size);
        if (files_in_dir < files_to_print) {
            printf("%s", mes);
            mes[0] = '\0';  // dont print last file!
        } else if (files_in_dir == files_to_print) {
            printf("...\n");
        }
        files_in_dir++;
    }
    printf("%s", mes); // print last file
    closedir(dir);
    
    printf("files in '%s': %d\n", path, files_in_dir);
    return files_in_dir;
}


int storage_write(const char* path, uint8_t *data, size_t len, size_t offset)
{
    // Open file for writing
    ESP_LOGI(TAG, "%s '%s'", __func__, path);
    FILE *f = fopen(path, "wb");
    if (f == NULL) {
        ESP_LOGE(TAG, "Failed to open '%s' for writing", path);
        return -1;
    } 
    if(fseek(f, offset, SEEK_SET) < 0) {
        fclose(f);
        ESP_LOGE(TAG, "fseek error with offset: %d", offset);
        return -1;
    }
    uint64_t start = esp_timer_get_time();  // Get time in microseconds since boot
    int bytes = fwrite(data, 1, len, f);
    if (len && 0 == bytes) {
        ESP_LOGW(TAG, "Write error. Try to write again!");
        vTaskDelay(100/portTICK_PERIOD_MS); // idle
        bytes = fwrite(data, 1, len, f);
    } 
    uint64_t end = esp_timer_get_time();
    uint64_t run_time_ms = (end - start)/1000;
    
    if (bytes != len) {
        ESP_LOGE(TAG, "Failed to write data to file (bytes: %d len: %d)", bytes, len);
    } else if (run_time_ms > 0) {
            ESP_LOGI(TAG, "Write OK: %llu ms bytes: %d rate: %llu bytes/s", 
                            run_time_ms, len, len/run_time_ms*1000);
    }
    fclose(f);
    return bytes;
}

int storage_read(const char* path, uint8_t *data, size_t len, size_t offset)
{
    // Open file for reading
    ESP_LOGI(TAG, "%s '%s'", __func__, path);
    FILE *f = fopen(path, "rb");
    if (f == NULL) {
        ESP_LOGE(TAG, "Failed to open '%s' for reading", path);
        return -1;
    } 
    if(fseek(f, offset, SEEK_SET) < 0) {
        fclose(f);
        ESP_LOGW(TAG, "fseek failed with offset: %d", offset);
        return -1;
    }
        
    int bytes = fread(data, 1, len, f);
    fclose(f);
    return bytes;
}

// posix file size
int storage_file_size(const char* path)
{
    // Check if destination file exists
    struct stat st = {};
    if (stat(path, &st) != 0) {
        ESP_LOGE(TAG, "%s File '%s' not found", __func__, path);
        return -1;
    }
    int size = st.st_size;

    return size;
}

// posix file remove
int storage_file_remove(const char* path)
{
    // Check if destination file exists
    struct stat st = {};
    if (stat(path, &st) != 0) {
        ESP_LOGE(TAG, "File '%s' Not Found", path);
        return -1;
    }
    
    //ESP_LOGI(TAG, "%s '%s'", __func__, path);
    // Delete it if it exists
    return unlink(path); // 0 - OK, -1 - ERROR
}

int storage_file_rename(const char* src, const char* dest)
{
    // Check if source file exists    
    struct stat st = {};
    if (stat(src, &st) != 0) {
        ESP_LOGE(TAG, "%s File '%s' not found", __func__, src);
        return -1;
    }
    
    // Check if destination file exists before renaming
    if (stat(dest, &st) == 0) {
        // Delete it if it exists
        unlink(dest);
    }

    // Rename original file
    if (rename(src, dest) != 0) {
        return -1;
    }
    return 0;
}


// --- NVS ---
#define STORAGE_NS  ("storage")

esp_err_t storage_nvs_init(void)
{
    // Initialize the default NVS partition.
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        // NVS partition was truncated and needs to be erased
        // Retry nvs_flash_init
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    return err;
}

// Deinitialize NVS storage for the default NVS partition
esp_err_t storage_nvs_deinit(void)
{
    // ESP_OK on success (storage was deinitialized)
    // ESP_ERR_NVS_NOT_INITIALIZED if the storage was not initialized prior to this call
    return nvs_flash_deinit();
}


esp_err_t storage_nvs_read_str(const char* key, char *dest, size_t len)
{
    nvs_handle_t my_handle;
    esp_err_t err = nvs_open(STORAGE_NS, NVS_READONLY, &my_handle);
    if (err == ESP_OK) {
        // Read
        size_t required_size = 0;
        err = nvs_get_str(my_handle, key, NULL, &required_size);
        if (err == ESP_OK) {
            if (required_size > len) {
                err = ESP_FAIL;
            } else {
                err = nvs_get_str(my_handle, key, dest, &required_size);
            }
        }
        // Close
        nvs_close(my_handle);
    }
    return err;
}


esp_err_t storage_nvs_write_str(const char* key, char *src)
{
    nvs_handle_t my_handle;
    esp_err_t err = nvs_open(STORAGE_NS, NVS_READWRITE, &my_handle);
    if (err == ESP_OK) {
        // Write
        err = nvs_set_str(my_handle, key, src);
        
        // Commit written value.
        // After setting any values, nvs_commit() must be called to ensure changes are written
        // to flash storage. Implementations may write to storage at other times,
        // but this is not guaranteed.
        err = nvs_commit(my_handle);

        // Close
        nvs_close(my_handle);
    }
    return err;
}

esp_err_t storage_nvs_read_i32(const char* key, int32_t *out_value)
{
    nvs_handle_t my_handle;
    esp_err_t err = nvs_open(STORAGE_NS, NVS_READONLY, &my_handle);
    if (err == ESP_OK) {
        err = nvs_get_i32(my_handle, key, out_value);
        nvs_close(my_handle);
    }
    return err;
}

esp_err_t storage_nvs_write_i32(const char* key, int32_t value)
{
    nvs_handle_t my_handle;
    esp_err_t err = nvs_open(STORAGE_NS, NVS_READWRITE, &my_handle);
    if (err == ESP_OK) {
        err = nvs_set_i32(my_handle, key, value);
        err = nvs_commit(my_handle);
        nvs_close(my_handle);
    }
    return err;
}

esp_err_t storage_nvs_read_u8(const char* key, uint8_t *out_value)
{
    nvs_handle_t my_handle;
    esp_err_t err = nvs_open(STORAGE_NS, NVS_READONLY, &my_handle);
    if (err == ESP_OK) {
        err = nvs_get_u8(my_handle, key, out_value);
        nvs_close(my_handle);
    }
    return err;
}

esp_err_t storage_nvs_write_u8(const char* key, uint8_t value)
{
    nvs_handle_t my_handle;
    esp_err_t err = nvs_open(STORAGE_NS, NVS_READWRITE, &my_handle);
    if (err == ESP_OK) {
        err = nvs_set_u8(my_handle, key, value);
        err = nvs_commit(my_handle);
        nvs_close(my_handle);
    }
    return err;
}
