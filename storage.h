#pragma once
#include <stdint.h>
#include "esp_err.h"

#ifdef __cplusplus
extern "C" {
#endif


// Create directory
int storage_dir_create(const char* path);
// Remove directory (must be empty)
int storage_dir_remove(const char* path);
int storage_dir_clear(const char* path);
int storage_dir_read(const char* path);

int storage_write(const char* path, uint8_t *data, size_t len, size_t offset);
int storage_read(const char* path, uint8_t *data, size_t len, size_t offset);
int storage_file_size(const char* path);
int storage_file_remove(const char* path);
int storage_file_rename(const char* src, const char* dest);


// Initialize the default NVS partition
esp_err_t storage_nvs_init(void);
// Deinitialize NVS storage for the default NVS partition
esp_err_t storage_nvs_deinit(void);

esp_err_t storage_nvs_read_str(const char* key, char *dest, size_t len);
esp_err_t storage_nvs_write_str(const char* key, char *src);

esp_err_t storage_nvs_read_i32(const char* key, int32_t *out_value);
esp_err_t storage_nvs_write_i32(const char* key, int32_t value);

esp_err_t storage_nvs_read_u8(const char* key, uint8_t *out_value);
esp_err_t storage_nvs_write_u8(const char* key, uint8_t value);

#ifdef __cplusplus
}
#endif
